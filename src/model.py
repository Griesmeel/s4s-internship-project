import pandas as pd
import data_preperation as dp
import pickle
from sklearn.tree import DecisionTreeRegressor

def make_model():
    input = data.drop("mean_travel_time", axis = 1)
    input.drop("daytime", axis = 1, inplace=True)
    output = data[["mean_travel_time"]]

    # create a regressor object 
    regressor = DecisionTreeRegressor(random_state = 0)  
    
    # fit the regressor with X and Y data 
    regressor.fit(input, output)

    pickle.dump(regressor, open('./model.pkl', 'wb'))
    model = pickle.load(open('./model.pkl', 'rb'))
    return model

data = dp.result_with_dummy_categories

model = make_model()

print("Preparing 2/2: Model calculated")