import pandas as pd
import sqlite3
from sqlite3 import Error


def insert_data(table, title):
    conn = None
    try:
        conn = sqlite3.connect("./database.db")

        # Write the dataframes to new SQLite tables
        table.to_sql(title, conn, if_exists="replace")

    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()


def get_data(table):
    # Read sqlite query results into a pandas DataFrame
    con = sqlite3.connect("./database.db")
    data = pd.read_sql_query("SELECT * from {}".format(table), con)

    con.close()

    # set first column as index
    index = list(data.columns)[0]
    data.set_index(index, inplace=True)

    return data
