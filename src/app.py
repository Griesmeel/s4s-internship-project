import numpy as np
import pandas as pd
from flask import Flask, request, jsonify, render_template
import plot

app = Flask(__name__)


@app.route('/')
def chart():
    return render_template('index.html')


@app.route('/predict', methods=['POST'])
def predict():

    daytime = request.form["time"]
    quarter = int(request.form["quarter"])
    weekend = int(request.form["weekend"])

    weekend_word = ('weekend' if weekend == 1 else 'weekday')

    if quarter == 1:
        quarter_words = "January till March"
    elif quarter == 2:
        quarter_words = "April till June"
    elif quarter == 3:
        quarter_words = "July till September"
    else:
        quarter_words = "October till December"

    bar = plot.create_plot(daytime, weekend, quarter)

    return render_template('index.html', plot=bar, prediction_text='Selected moment: {}, on a {} from {}'.format(daytime, weekend_word, quarter_words))


if __name__ == "__main__":
    app.run(debug=False)
