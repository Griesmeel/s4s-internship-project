import pandas as pd
import numpy as np
from os import listdir
import database_helper as dh


def process_file(filepath):
    # put csv file in dataframe
    df = pd.read_csv("../data/" + filepath)

    # add columns quarter and year
    file_title = filepath.split("-")
    df["year"] = int(file_title[2])
    df["quarter"] = int(file_title[3])

    # thats it for the weekly aggregate tables
    if file_title[4] == "WeeklyAggregate.csv":
        return df

    # the following part is only executed for tables that differentiate between weekends
    else:
        # 1, 0 or NaN when it unknown
        weekend_value = np.nan
        if file_title[4] == "OnlyWeekends":
            weekend_value = 1
        elif file_title[4] == "OnlyWeekdays":
            weekend_value = 0
        df["weekend"] = weekend_value

        return df


def read_area_names():
    df = pd.read_json("../data/amsterdam_wijk.json")
    area_names = pd.json_normalize(df["features"])[
        ["properties.MOVEMENT_ID", "properties.WK_NAAM"]
    ]
    area_names["properties.MOVEMENT_ID"] = (
        area_names["properties.MOVEMENT_ID"].astype(str).astype(int)
    )

    area_names.columns = ["id", "name"]
    # replace missing values
    area_names.at[150, "name"] = "Ijmuiden"
    area_names.at[126, "name"] = "Ransdorp"
    area_names.at[134, "name"] = "Bloemendaal"
    area_names.at[139, "name"] = "Waterkeringpad, Diemen"
    area_names.at[151, "name"] = "Zandvoort"
    area_names.at[6, "name"] = "Spaarndam"
    area_names.at[11, "name"] = "Schiphol-Rijk"
    area_names.at[55, "name"] = "Oostzaan"
    area_names.at[60, "name"] = "Ouderkerk aan de Amstel"    

    # remove wijk xx from some the strings
    area_names.loc[(area_names["name"].str.startswith("Wijk")), "name"] = area_names[
        "name"
    ].str.slice(start=8)

    # set index
    area_names.set_index("id", inplace=True)
    return area_names


# function that removes areas we have too little data from
def remove_areas(data):
    amountbysource = data.groupby(["sourceid"]).count()["dstid"]
    amountbydest = data.groupby(["dstid"]).count()["sourceid"]
    totalbyarea = amountbysource.add(amountbydest)

    lower_limit = np.percentile(totalbyarea, 5)
    areaswekeep = totalbyarea[totalbyarea > lower_limit].index

    new_data = data[data["sourceid"].isin(areaswekeep)]
    new_data = new_data[new_data["dstid"].isin(areaswekeep)]

    return new_data


def hours_to_daytimes(hour):
    if 7 <= hour < 10:
        return "AM Peak"
    elif 10 <= hour < 16:
        return "Midday"
    elif 16 <= hour < 19:
        return "PM Peak"
    elif 19 <= hour < 23:
        return "Evening"
    elif 23 == hour or hour < 7:
        return "Early Morning"
    return "Unknown"


def dow_to_bool(day):
    if 1 <= day < 6:
        return 0
    elif day == 6 or day == 7:
        return 1
    return np.nan


def prepare_for_model(data):
    # Categories to numerical
    new_data = pd.concat(
        [data, pd.get_dummies(data["daytime"], prefix="daytime")], axis=1
    )

    # drop columns that are not needed for the model
    new_data.drop("busyness", axis=1, inplace=True)
    new_data.drop("standard_deviation_travel_time", axis=1, inplace=True)
    new_data.drop("geometric_mean_travel_time", axis=1, inplace=True)
    new_data.drop("geometric_standard_deviation_travel_time", axis=1, inplace=True)
    return new_data


def remove_outliers(data):
    Q1 = data["mean_travel_time"].quantile(0.25)
    Q3 = data["mean_travel_time"].quantile(0.75)
    IQR = Q3 - Q1
    lower_bound = Q1 - (1.5 * IQR)
    upper_bound = Q3 + (1.5 * IQR)
    data = data[(data["mean_travel_time"] > lower_bound) & (data["mean_travel_time"] < upper_bound)]

    return data


def prepare_data_hours(raw_data_hours):
    # substitute hours for daytime categories
    raw_data_hours["daytime"] = raw_data_hours["hod"].apply(hours_to_daytimes)
    data_hours = raw_data_hours.drop(["hod"], axis=1)

    return data_hours


def prepare_data_days(raw_data_days):
    # substitute days for weekend bool
    raw_data_days["weekend"] = raw_data_days["dow"].apply(dow_to_bool)
    raw_data_days.drop(["dow"], axis=1, inplace=True)

    # for each area pair calculate mean_travel_time for weekends and non weekends
    pair_day_mean_table = (
        raw_data_days.groupby(["sourceid", "dstid", "weekend"])
        .mean()["mean_travel_time"]
        .reset_index()
    )
    pair_day_mean_table.rename(
        columns={"mean_travel_time": "pair_day_mean"}, inplace=True
    )
    return pair_day_mean_table


def combine_dataframes(raw_data_hours, pair_day_mean_table):
    # prepare hours dataframe
    data_hours = prepare_data_hours(raw_data_hours)

    # merge the two dataframes
    result = pd.merge(
        data_hours, pair_day_mean_table, how="left", on=["sourceid", "dstid", "weekend"]
    )

    # add busyness per row for analysis purposes
    result["busyness"] = result["mean_travel_time"] / result["pair_day_mean"]

    # there are 4 rows that miss a value, drop those
    result.dropna(inplace=True)

    # remove outliers in busyness with the IQR method
    result = remove_outliers(result)

    # remove areas that we have too little data from
    result = remove_areas(result)

    return result


# list all filepaths we want to read
filepaths_hours = [f for f in listdir("../data") if f.endswith("HourlyAggregate.csv")]
filepaths_days = [f for f in listdir("../data") if f.endswith("WeeklyAggregate.csv")]

# concat all the relevant files
raw_data_hours = pd.concat(map(process_file, filepaths_hours))
raw_data_days = pd.concat(map(process_file, filepaths_days))

# prepare the pair_day_mean table, and insert into database
pair_day_mean_table = prepare_data_days(raw_data_days)
dh.insert_data(pair_day_mean_table, "pair_day_mean")

# prepare area name table and insert into db
area_names = read_area_names()
dh.insert_data(area_names, "area_names")

# prepare the dataframes for machine learning
result = combine_dataframes(raw_data_hours, pair_day_mean_table)
result_with_dummy_categories = prepare_for_model(result)

print("Preparing 1/2: Data prepared")