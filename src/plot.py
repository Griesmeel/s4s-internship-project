import numpy as np
import pandas as pd
import plotly
import plotly.graph_objs as go
import json
import pickle
from plotly.subplots import make_subplots
import database_helper as dh
import plotly.express as px


model = pickle.load(open('./model.pkl', 'rb'))
pair_mean_table = dh.get_data("pair_day_mean")
area_names = dh.get_data("area_names")


def busyness_function(row, daytime):
    sourceid = row["sourceid"]
    dstid = row["dstid"]
    weekend = row["weekend"]
    pair_day_mean = row["pair_day_mean"]
    year = row["year"]
    quarter = row["quarter"]
    features = [sourceid, dstid, year, quarter,
                weekend, pair_day_mean] + daytime
    prediction = model.predict(np.array([features]))
    busyness = (prediction[0] / pair_day_mean) * 100 - 100
    return np.round(busyness, 3)


def prediction_function(row):
    early_morning = busyness_function(row, [0, 1, 0, 0, 0])
    am_peak = busyness_function(row, [1, 0, 0, 0, 0])
    midday = busyness_function(row, [0, 0, 0, 1, 0])
    pm_peak = busyness_function(row, [0, 0, 0, 0, 1])
    evening = busyness_function(row, [0, 0, 1, 0, 0])
    return pd.Series([early_morning, am_peak, midday, pm_peak, evening])


def create_daytime_table(daytime, weekend, quarter):
    year = 2020
    n = 10

    # Create X values for the model
    busyness_table = pair_mean_table
    busyness_table = busyness_table[busyness_table["weekend"] == weekend]
    busyness_table["year"] = year
    busyness_table["quarter"] = quarter

    table = busyness_table.groupby(["sourceid"]).mean()
    table.reset_index(inplace=True)

    # Calculate mean_travel_time (Y value) for each area, and the percentual increase compared to the day mean
    table[["Early Morning", "AM Peak", "Midday", "PM Peak", "Evening"]
          ] = table.apply(prediction_function, axis=1)

    # Select n areas with highest increase at the specified daytime
    topten = table.nlargest(n, daytime)

    # Merge with the area_names table
    topten = pd.merge(topten, area_names, left_on='sourceid', right_on='id')
    topten = topten[["name", "Early Morning",
                     "AM Peak", "Midday", "PM Peak", "Evening"]]

    return topten


def create_plot(daytime, weekend, quarter):
    topten = create_daytime_table(daytime, weekend, quarter)
    cols = px.colors.sequential.thermal

    # Create two plots next to each other
    fig = make_subplots(cols=2, subplot_titles=[
                        'The 10 biggest bottlenecks', 'The same areas per daytime'])

    # Bar plot (left)
    barx = np.array(list(topten["name"]))
    bary = np.array(topten[daytime].tolist())

    fig.add_trace(go.Bar(
        x=barx,
        y=bary,
        showlegend=False,
        marker_color=cols
    ))
    
    fig['layout']['yaxis2'].update(ticksuffix = "%")

    fig.update_layout(
        yaxis_title="Increase in travel time",
        yaxis_range=[0, bary[0] + 2],
        yaxis_ticksuffix = "%"
    )

    # Line plot (right)
    x2 = np.array(["Early Morning", "AM Peak", "Midday", "PM Peak", "Evening"])
    for x in range(10):
        y = np.array(topten.iloc[x][1:6])
        fig.add_trace(go.Scatter(x=x2, y=y, line=dict(
            width=2, color=cols[x]), name=topten.iloc[x]["name"]), row=1, col=2)

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return graphJSON
