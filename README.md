# Internship-Christa

## Install dependencies
```sh
conda env create -f environment.yml
```

## Prepare data & calculate model
```sh
cd src
python model.py
```

## Run de app
```sh
python app.py
```
open http://localhost:5000 in your browser

## Capture
![picture](capture.PNG)
